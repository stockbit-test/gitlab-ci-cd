#!/bin/bash

set -e

RED="\033[0;31m" # Error / Failed
GREEN="\033[0;32m" # Success / Passed
YELLOW="\033[0;33m" # Warning
BLUE="\033[0;34m" # Information
PURPLE="\033[0;35m" # Other
CYAN="\033[0;36m" # Other 1
NN="\033[0m" # End
