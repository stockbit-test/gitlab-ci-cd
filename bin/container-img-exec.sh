#!/bin/bash
#
## This is a shell script for exec command to Container Image
##
## some variable needs to be exported as Environment variable for this script
##
## Base image : debian
## Dependencies : podman
##
## Variables that must be supplied to use this script 
## Required :
## - CI_REGISTRY = Predefined variables (ref : https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
## - CI_REGISTRY_USER = Predefined variables (ref : https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
## - CI_REGISTRY_PASSWORD = Predefined variables (ref : https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
## - CI_REGISTRY_IMAGE = Predefined variables (ref : https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
## - IMAGE_BUILD_LIST = Folder of your storing dockerfile image, this will also become your Image Name.
## - IMAGE_BUILD_PATH = Path to list folder of dockerfiles
##
## Usage:
## ./container-img-exec.sh [build|logout]

source "${BIN_PATH}/.base.sh"

# Define Functions Script
## Function when command missing
report_command_missing() {
  printf "${RED}no command statement${NN}\n"
  exit 1
}

## Function when command missing
report_option_missing() {
  printf "${RED}no option statement${NN}\n"
  exit 1
}

## Function to do Docker login
exec_docker_login() {
  printf "Login to Container Registry : ${BLUE}${CI_REGISTRY}${NN}\n"

  docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
}

# Main script
case $1 in
  build)
    exec_docker_login

    for IMAGE_BUILD_LOOP in ${IMAGE_BUILD_LIST}
    do
      export IMAGE_BUILD_DIR=${IMAGE_BUILD_PATH}/${IMAGE_BUILD_LOOP}
      export IMAGE_NAME=$(basename "${IMAGE_BUILD_DIR}")

      if [[ -f "${IMAGE_BUILD_DIR}/version" ]]
      then
        export IMAGE_VERSION=$(cat ${IMAGE_BUILD_DIR}/version)
      else
        export IMAGE_VERSION=${CI_COMMIT_SHA}
      fi

      printf "Build Image : ${BLUE}${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${IMAGE_VERSION}${NN}\n"
      docker build -f ${IMAGE_BUILD_DIR}/Dockerfile \
        -t ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${IMAGE_VERSION} .
      docker push ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${IMAGE_VERSION}
    done
    ;;
  
  login)
    exec_docker_login
    ;;

  logout)
    printf "Logout from Container Registry : ${BLUE}${CI_REGISTRY}${NN}\n"
    
    docker logout ${CI_REGISTRY}
    ;;
  
  *)
    report_command_missing
    ;;
esac
