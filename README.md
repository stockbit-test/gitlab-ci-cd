# Gitlab CI CD

## Description

This repository is a statement submission for completion of CI/CD related Test. where the tasks that must be done is :
```
Tuliskan 1 Dockerfile config + 1 CI/CD Pipeline YAML file.
* Preferably, YAML filenya utk CI/CD Gitlab. Tapi kalau lebih familiar dengan Repo lain eg. Bitbucket/Github, silakan menggunakan syntax untuk repo itu.
``` 

## Overview Results

To-Do list :  
- [x] `YAML filenya akan melakukan deployment dari sebuah docker image yg dibentuk oleh sebuah Dockerfile`  
  Statement Results :
    - `.gitlab-ci.yml` configuration ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/blob/main/.gitlab-ci.yml)).
    - `yml`/`yaml` templates & jobs ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/tree/main/pipelines)).
    - `binary` scripts ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/tree/main/bin))
    - `Pipelines` results ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/pipelines/383039892))
- [x] `Dockerfile-nya perlu simply install Nginx dan memasukkan file dari repo bernama "hello.txt" ke dalam folder "/var/www/". Let's assume folder itu adalah folder utama yang akan dibaca Nginx sebagai web server`  
  Statement Results :
    - `Dockerfile` ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/blob/main/dependencies/dockerfiles/web/Dockerfile)).
    - `Job Build` results ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/jobs/1652140136)).
    - `Docker image` results ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/container_registry/2345533))
- [x] `Docker imagenya kemudian perlu diinstall di sebuah server EC2 di AWS dan dijalankan`  
  Statement Results :
    - `docker-compose.yml` for requirement install docker image ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/blob/main/dependencies/docker-compose/docker-compose.yml))
    - `Job Deploy` results ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/jobs/1652140139)).
    - `Environment Deploy` results ([link](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/environments)).
    - `Servers Instances` can accessed : http://103.55.38.163/. 

## How it Works

### Prerequisite

- Because the final target of this Gitlab CI CD is to be able to deploy docker services to an EC2 server instance, then I assume that the initial prerequisite must be done is EC2 server instance already provisioned with the following instance requirements : 
  - OS Linux
  - Docker Installed (version `20.10.9`)
  - Docker Compose Installed (version `1.29.2`)

### Guides

- On this repository open `Settings` > `CI/CD` & expand `Variables`
- Add required variables for Deploys
  Type          | Key               | Value            | Description
  ---           | ---               | ---              | ---
  Variable      | EC2_INSTANCE_IP   | xxx.xxx.xxx.xxx  | Host IP EC2 Instance can be SSH by runner
  Variable      | EC2_INSTANCE_USER | ec2-user         | SSH User EC2 Instance
  File          | EC2_INSTANCE_KEY  | -----BEGIN OPENSSH PRIVATE KEY----- | SSH Private Key EC2 Instance
- Push Your code to `DEFAULT_BRANCH` =~ `main`/`master`
- Then, pipelines whill triggered : [CI/CD > Pileninse](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/pipelines)
  ![Pipelines](img/1.png)
- After Pipelines finished, Go to [Deployments > Environments](https://gitlab.com/stockbit-test/gitlab-ci-cd/-/environments) and click Open App
  ![Environments](img/2.png)
- Web Results !
  ![Web Results](img/3.png)